<!DOCTYPE html>
<html>
<head>
    <title>Ajouter un agent</title>
    <link rel="stylesheet" href="CSS/styles.css">
</head>
<body>

<?php
$error = null; # valeur par default null
$success = null;
$email = null;
$prenom = null;
$nom = null;

if ($_SERVER['REQUEST_METHOD'] === 'POST') { # "===" augmente la sécurité et "post" évite de voir les variables dans l'URL.
    # si aucune des 3 variables n'est vide alors on les valide.
    if (!empty($_POST['email']) && !empty($_POST['prenom']) && !empty($_POST['nom'])) {
        $email = $_POST['email'];
        $prenom = $_POST['prenom'];
        $nom = $_POST['nom'];

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $data = "Email: $email, Prénom: $prenom, Nom: $nom" . PHP_EOL; # écrire les données séparées par des virgules et mètre le tout à la ligne.
            $file = __DIR__ . DIRECTORY_SEPARATOR . 'emails' .DIRECTORY_SEPARATOR . date('Y-m-d') . '.txt'; # on enregistre dans le dossier principal un dossier email et ensuite on y enregistre un fichier qui aura le nom de la date (année mois jour) au format .txt
            file_put_contents($file, $data, FILE_APPEND); # écrire les données à la fin du fichier sans supprimer les anciennes données.
            $success = "Enregistrement réussi."; # message quand l'enregistrement est reussi.
        } else {
            $error = "Email invalide"; # message si l'email n'est pas au bon format.
        }
    } else {
        $error = "Veuillez remplir tous les champs."; # message si une des 3 variables est vide. 
    }
}
?>


<h1>Ajouter un agent</h1>

<?php if ($error): # Si erreur, alors alert-danger?>
    <div class="alert alert-danger">
        <?= htmlspecialchars($error) ?> <!-- exclure les caractères spéciaux pour éviter l'injection de code -->
    </div>
<?php endif; # sinon fin de la condition?>

<?php if ($success): # Si pas d'erreur, alors alert-succes?>
    <div class="alert alert-success">
        <?= htmlspecialchars($success) ?>
    </div>
<?php endif; # sinon fin de la condition?>

<form action="/AjouterUnAgent.php" method="post"> <!-- formulaire -->

        <div class="form-group">
            <label for="email">Email*:</label>
            <input type="email" name="email" id="email" placeholder="Entrer votre email*" required>
        </div>

    <div class="form-inline"> <!-- les labels Prenom et Nom et leurs input sont dans la même class "form-inline pour etre l'un a coté de l'autre, le label email sera donc au dessus -->    
        <div class="form-group">
            <label for="prenom">Prénom*:</label> <!-- Nom du label -->  
            <input type="text" name="prenom" id="prenom" placeholder="Entrer votre prénom*" required> <!-- caracteristque de l'input : "name" pour l'utilisation des données, "id" pour repérer et mise en page, "placeholder" pour indication dans la case input, "required" pour obligation de remplir le champs -->  
        </div>
            <div class="form-group">
                <label for="nom">Nom*:</label>
                <input type="text" name="nom" id="nom" placeholder="Entrer votre nom*" required>
            </div>
    </div>

    <div class="form-actions">
        <button type="reset">Annuler</button>
        <button type="submit">Créer</button>
    </div>
</form>
</body>
</html>
